package com.wcq.dessert.demo;

import com.wcq.dessert.core.CallPoint;
import com.wcq.dessert.core.Port;
import com.wcq.dessert.core.RPCProxyBase;

/**
 * 根据StageService生成的
 */
public class StageServiceProxy extends RPCProxyBase {


    public final static int METHOD_KEY_DOSOME1 = 1;
    public final static int METHOD_KEY_DOSOME2 = 2;

    public static StageServiceProxy inst() {
        StageServiceProxy proxy = new StageServiceProxy();
        // 先写死，这个应该配置在分布式配置中
        proxy.remote = new CallPoint("node", "port1", "stageService");
        return proxy;
    }

    /**
     * 反射StageService，自然知道doSome1的形参类型
     * @param a
     * @param b
     */
    public void doSome1(int a, int b){
        Port port = Port.getCurrent();
        port.call(remote, METHOD_KEY_DOSOME1, new Object[]{a, b});
    }

    public void doSome2(int a, int b){
        Port port = Port.getCurrent();
        port.call(remote, METHOD_KEY_DOSOME2, new Object[]{a, b});
    }
}
