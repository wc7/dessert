package com.wcq.dessert.demo;

import co.paralleluniverse.fibers.SuspendExecution;
import com.wcq.dessert.core.Port;
import com.wcq.dessert.core.RpcMethod;
import com.wcq.dessert.core.Service;
import com.wcq.dessert.support.Log;


public class StageService extends Service {
    public StageService(String serviceName, Port port) {
        super(serviceName, port);
    }

    @RpcMethod
    public void doSome1(int a, int b) throws InterruptedException, SuspendExecution {
        Log.console("stageService doSome1 收到 {}----------------------------------------", a);
        String result = FriendServiceProxy.inst().doSome2(1, "2");
        Log.console("stageService 完成 {},结果为{}-------------------------------", a, result);
//        result = FriendServiceProxy.inst().doSome2(1, "2");
//        Log.console("stageService 完成 {},结果为{}-------------------------------", a, result);

//        StageServiceProxy.inst().doSome2(a, 2);

    }

    @RpcMethod
    public void doSome2(int a, int b) throws InterruptedException, SuspendExecution {
        Log.console("stageService doSome2收到 {}----------------------------------------", a);

    }
}


