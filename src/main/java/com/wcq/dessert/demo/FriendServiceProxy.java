package com.wcq.dessert.demo;

import co.paralleluniverse.fibers.SuspendExecution;
import com.wcq.dessert.core.CallPoint;
import com.wcq.dessert.core.Port;
import com.wcq.dessert.core.RPCProxyBase;

public class FriendServiceProxy extends RPCProxyBase {


    public final static int METHOD_KEY_DOSOME2 = 1;

    public static FriendServiceProxy inst() {
        FriendServiceProxy proxy = new FriendServiceProxy();
        // 先写死，这个应该配置在分布式配置中
        proxy.remote = new CallPoint("node", "port2", "friendService");
        return proxy;
    }

    /**
     * 反射FriendService，自然知道doSome2的形参类型，以及返回的参数类型
     * @param a
     * @param c
     * @return
     * @throws SuspendExecution
     * @throws InterruptedException
     */
    public String doSome2(int a, String c) throws SuspendExecution, InterruptedException {
        Port port = Port.getCurrent();

        return (String)port.callWait(remote, METHOD_KEY_DOSOME2, new Object[]{a, c});
    }

}