package com.wcq.dessert.demo;

import com.wcq.dessert.core.Port;
import com.wcq.dessert.core.RpcMethod;
import com.wcq.dessert.core.Service;
import com.wcq.dessert.support.Log;
import com.wcq.dessert.support.Utils;


public class FriendService extends Service {

    public FriendService(String serviceName, Port port) {
        super(serviceName, port);
    }

    @RpcMethod
    public String doSome2(int a, String c) {
        Log.console("{} 执行方法doSome2 {} {} ------------------------------", name, a, c );

//        StageServiceProxy.inst().doSome2(1, 1);

        return Utils.createStr("从FriendService返回的消息{} {}", a, c);
    }
}
