package com.wcq.dessert.demo;

import com.wcq.dessert.core.RPCImplBase;
import com.wcq.dessert.core.Service;
import com.wcq.dessert.support.Function1;
import com.wcq.dessert.support.Function2;
import com.wcq.dessert.support.ReturnFunction1;

/**
 * 根据StageService生成的
 */
public class StageServiceImpl extends RPCImplBase {
    @Override
    public Object getMethodFunction(Service serv, int methodKey) {
        StageService service = (StageService) serv;
        switch (methodKey){
            case 1:
                return (Function2<Integer, Integer>)service::doSome1;
            case 2:
                return (Function2<Integer, Integer>)service::doSome2;
        }
        return null;
    }
}
