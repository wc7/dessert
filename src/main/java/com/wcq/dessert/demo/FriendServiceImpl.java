package com.wcq.dessert.demo;

import com.wcq.dessert.core.RPCImplBase;
import com.wcq.dessert.core.Service;
import com.wcq.dessert.support.ReturnFunction2;

/**
 * 生成的
 */
public class FriendServiceImpl extends RPCImplBase {
    @Override
    public Object getMethodFunction(Service serv, int methodKey) {
        FriendService service = (FriendService) serv;
        switch (methodKey){
            case 1:
                return (ReturnFunction2<String, Integer, String>)service::doSome2;
        }
        return null;
    }
}
