package com.wcq.dessert.core;

/**
 * 一次rpc请求的结构
 */
public class Call {
    /** call=1  callResult=2 */
    public int type;

    /** ------------从哪来-------------*/
    public String fromNode;
    public String fromPort;

    /** ------------从哪去-------------*/
    public CallPoint to;

    /** to service后调用哪个方法 */
    public int methodKey;
    /** to service后调用方法的参数 */
    public Object[] methodParam;
    /** to service后调用的方法是否有返回值 */
    public boolean needResult;
    /** 返回的值为 */
    public Object result;

    /** 请求后回调contextid */
    public long callbackId;


    /** 这个啥也不是，序列号，debug用的 */
    public int sn;


    public Call createReturn() {
        Call callTo = new Call();
        callTo.type = 2;

        callTo.to = new CallPoint("node", this.fromPort, "");
        callTo.callbackId = this.callbackId;
        callTo.sn = this.sn;

        callTo.fromPort = this.to.portId;
        return callTo;
    }
}
