package com.wcq.dessert.core;

/**
 * Call的上下文
 */
public class CallContext {
    public long id;
    Call call;
    public Service service;
    public PortFiber fiber;

    public CallContext(long id, Call call, Service service, PortFiber fiber) {
        this.id = id;
        this.call = call;
        this.service = service;
        this.fiber = fiber;
    }
}
