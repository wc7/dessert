package com.wcq.dessert.core;

import co.paralleluniverse.fibers.Fiber;
import co.paralleluniverse.fibers.SuspendExecution;
import co.paralleluniverse.strands.SuspendableRunnable;

/**
 * 主协程
 */
public class PulseFiber  implements SuspendableRunnable {
    Port port;
    Fiber fiber;
    public PulseFiber(Port port){
        this.port = port;
        // 使用port的调度器启动，保证与PortFiber在同一个线程
        fiber = new Fiber(port.fiberScheduler, this);
        fiber.start();
    }

    @Override
    public void run() throws SuspendExecution, InterruptedException {
        port.init();
        port.pulse();
    }

    public void unpark() {
        fiber.unpark();
    }
}
