package com.wcq.dessert.core;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 *
 * 远程调用结点信息
 */  
public class CallPoint {
	public String nodeId;
	public String portId;
	public Object servId;

	/**
	 * 构造函数
	 * @param nodeId
	 * @param portId
	 * @param servId
	 */
	public CallPoint(String nodeId, String portId, Object servId) {
		this.nodeId = nodeId;
		this.portId = portId;
		this.servId = servId;
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
					.append("nodeId", nodeId)
					.append("portId", portId)
					.append("servId", servId)
					.toString();
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getPortId() {
		return portId;
	}

	public void setPortId(String portId) {
		this.portId = portId;
	}

	public Object getServId() {
		return servId;
	}

	public void setServId(Object servId) {
		this.servId = servId;
	}
}
