package com.wcq.dessert.core;

import java.util.HashMap;
import java.util.Map;

public class Node {
    private String name;
    private Map<String, Port> ports = new HashMap<>();

    public Node(String name){
        this.name = name;
    }

    public void addCall(Call call){
        Port port = ports.get(call.to.getPortId());
        port.addCall(call);
    }

    public void addPort(Port port){
        ports.put(port.getName(), port);
    }

    public Port getPort(String name) {
        return ports.get(name);
    }

    public String getName() {
        return name;
    }

    public void dispatch(Call call) {
        // 节点路由，目前只有一个节点，所以不考虑进程间通信

        // 本地路由
        getPort(call.to.portId).addCall(call);
    }
}
