package com.wcq.dessert.support;

/**
 * 
 *
 * 接受1个参数的返回函数
 * @param <R, T1>
 */
@FunctionalInterface
public interface ReturnFunction1<R, T1> {
	
	R apply(T1 t1);

}
