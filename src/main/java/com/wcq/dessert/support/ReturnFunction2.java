package com.wcq.dessert.support;

/**
 * 
 *
 * 接受1个参数的返回函数
 * @param <R, T1, T2>
 */
@FunctionalInterface
public interface ReturnFunction2<R, T1, T2> {
	
	R apply(T1 t1, T2 t2);

}
