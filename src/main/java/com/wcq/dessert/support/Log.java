package com.wcq.dessert.support;

import co.paralleluniverse.fibers.Fiber;

public class Log {
    public static void console(String str, Object... args){
        if (Fiber.currentFiber() == null){
            System.out.println("[" + Thread.currentThread().getName() +"]  [ thread ]  "+ Utils.createStr(str, args));
        }else{
            System.out.println("[" + Thread.currentThread().getName() +"]  [" + Fiber.currentFiber().getName() + "]"+ Utils.createStr(str, args));

        }
    }
}
