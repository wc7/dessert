package com.wcq.dessert.support;

import co.paralleluniverse.fibers.SuspendExecution;

/**
 * 
 *
 * 接受1个参数的函数
 * @param <T1>
 */
@FunctionalInterface
public interface Function1<T1> {
	
	void apply(T1 t1) throws SuspendExecution, InterruptedException;

}
