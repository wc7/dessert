package com.wcq.dessert;

import com.wcq.dessert.core.Call;
import com.wcq.dessert.core.Node;
import com.wcq.dessert.core.Port;
import com.wcq.dessert.core.Service;
import com.wcq.dessert.demo.FriendService;
import com.wcq.dessert.demo.StageService;
import com.wcq.dessert.demo.StageServiceProxy;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Node node = new Node("node");

        Port port1 = new Port("port1");
        Service stageService = new StageService("stageService", port1);
        port1.addService(stageService);
        port1.startUp(node);

        Port port2 = new Port("port2");
        Service friendService = new FriendService("friendService", port2);
        port2.addService(friendService);
        port2.startUp(node);

        // 以上的node port service的关系应该配置在分布式配置中


        Thread.sleep(1000);
        clientSend(port1);


        try {
            Thread.sleep(11111111);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 假装这是个客户端输入，数据入口
     * @param port1
     */
    private static void clientSend(Port port1) {

        StageServiceProxy proxy = StageServiceProxy.inst();
        for (int i = 0; i < 1000; i++) {
            Call call = new Call();

            call.type = 1;

            call.to = proxy.getRemote();
            call.sn = i + 1;
            call.methodKey = StageServiceProxy.METHOD_KEY_DOSOME1;
            call.methodParam = new Object[]{call.sn, 2};
            port1.addCall(call);
        }


    }
}
